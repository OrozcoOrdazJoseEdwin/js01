function Factory() {
	function _crear(obj) {
		if(obj.tipo === "Alumno") {
			return new Alumno(obj);
		}
		if(obj.tipo === "Grupo") {
			return new Grupo(obj);
		}
		if(obj.tipo === "Docente") {
			return new Docente(obj);
		}
		if(obj.tipo === "Materia") {
			return new Materia(obj);
		}
	}
	return {
		"crear" : _crear
	}
}
