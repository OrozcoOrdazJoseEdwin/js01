function Alumno (obj){
    var _id = obj.id;
    var _nombre = obj.nombre;
    var _aPaterno = obj.aPaterno;
    var _aMaterno = obj.aMaterno;
    var _genero = obj.genero;
    var _calificaciones = [];
    var _tipo = "Alumno";
    
    function _getId() {
		return _id;
	}
	function _getNombre() {
		return _nombre;
	}
	function _getPaterno() {
		return _aPaterno;
	}
	function _getMaterno() {
		return _aMaterno;
	}
	function _getGenero() {
		return _genero;
	}
	function tipo() {
		return _tipo;
	}
	function _toString () {
		return "id: " + _id + "\n" +
			"nombre: " + _nombre + "\n" +
			"genero: " + _genero + "\n" +
			"paterno: " + _aPaterno + "\n" +
			"materno: " + _aMaterno ;
	}
	return {
		"id" : _getId(),
		"nombre" : _getNombre(),
		"aPaterno" : _getPaterno(),
		"aMaterno" : _getMaterno(),
		"toString" : _toString(),
		"tipo" : tipo()
	}
}
