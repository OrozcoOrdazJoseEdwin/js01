function Grupo (obj) {
	var _id = obj.id;
	var _clave = obj.clave;
	var _nombre = obj.nombre;
	var _alumnos = obj.alumnos;
	var _materiasPorDocente = [];
	var _tipo = "Grupo";
	
	function tipo() {
		return _tipo;
	}
	function toString() {
		return "id: " + _id + "\n" +
		"clave" + _clave + "\n" +
		"nombre" + _nombre;
	}
	return {
		"id" : _id,
		"clave" : _clave,
		"nombre" : _nombre,
		"alumnos" : _alumnos,
		"materias" : _materias,
		"tipo" : tipo()
	}
}
