var ob1 = {"id":12345,"nombre":"Edwin","aPaterno":"Orozco","aMaterno":"Ordaz","tipo":"Alumno","genero":"M"};
var ob2 = {"id":1000,"nombre":"Edwin profe","aPaterno":"Orozco","aMaterno":"Ordaz","tipo":"Docente","rfc":"as123"};
var ob3 = {"id":12378,"nombre":"mari","aPaterno":"sansa","aMaterno":"core","tipo":"Alumno","genero":"M"};
var factory = new Factory();
var alumno1 = factory.crear(ob1);
var alumno2 = factory.crear(ob3);
var docente = factory.crear(ob2);
var ob4 = {
	"id":12,
	"clave":"scr1",
	"nombre":"matrix",
	"alumnos": [alumno1,alumno2],
	"docentes":[docente],
	"materias":[],
	"tipo":"Grupo"
};
var grupo = factory.crear(ob4);
var managerGrup = new ManejadorGrupo(grupo);
console.log("---listar alumnos-----");
managerGrup.enlistarAlumnos();
var alumnoNuevo = factory.crear({
		"id" : 4312,
		"nombre" : "nuevo",
		"aPaterno" : "san",
		"aMaterno" : "sama",
		"tipo" : "Alumno"
});
console.log("---agregar alumno al grupo: "+alumnoNuevo.nombre);
managerGrup.agregarAlumno(alumnoNuevo);
managerGrup.enlistarAlumnos();
console.log("---eliminar alumno por id : "+12345);
managerGrup.eliminarAlumno(12345);
managerGrup.enlistarAlumnos();
console.log("---pruebas-------");

