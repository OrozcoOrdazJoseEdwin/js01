function Docente (obj){
    var _id = obj.id;
    var _rfc = obj.rfc;
    var _nombre = obj.nombre;
    var _aPaterno = obj.aPaterno;
    var _aMaterno = obj.aMaterno;
    var _tipo = "Docente";
    
    function _getId() {
		return _id;
	}
	function _getRFC() {
		return _rfc;
	}
	function _getNombre() {
		return _nombre;
	}
	function _getPaterno() {
		return _aPaterno;
	}
	function _getMaterno() {
		return _aMaterno;
	}
	function tipo() {
		return _tipo;
	}
	function _toString () {
		return "id: " + _id + "\n" +
			"nombre: " + _nombre + "\n" +
			"rfc: " + _rfc + "\n" +
			"paterno: " + _aPaterno + "\n" +
			"materno: " + _aMaterno ;
	}
	return {
		"id" : _getId(),
		"rfc" : _getRFC(),
		"nombre" : _getNombre(),
		"aPaterno" : _getPaterno(),
		"aMaterno" : _getMaterno(),
		"toString" : _toString(),
		"tipo" : tipo()
	}
}
