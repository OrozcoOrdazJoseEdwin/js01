function ManejadorGrupo (grupo) {
	var _grupo = grupo;
	function _agregarAlumno(alumno) {
		var num_alumnos = _grupo['alumnos'].length;
		if(num_alumnos < 30) {
			_grupo['alumnos'].push(alumno);
		} else {
			console.log("no se pueden agregar mas de 30 alumnos");
		}
	}
	function _agregarDocente(docente) {
		var num_docentes = _grupo['docentes'].length;
		if(num_docentes < 7) {
			_grupo['docentes'].push(docente);
		} else {
			console.log("no se pueden agregar mas de 7");
		}
	}
	function _agregarMateria(materia) {
		var num_materias = _grupo['materias'].length;
		if(num_materias < 7) {
			grupo['materias'].push(materia);
		} else {
			console.log("no se pueden agregar mas de 7");
		}
	}
	function _eliminarAlumno(alumno_id) {
		var num_alumnos = _grupo['alumnos'].length;
		if (num_alumnos == 0 ) {
			return;
		}
		var alumno = _grupo['alumnos'];
		var arregloAux = [];
		for(var i = 0; i < num_alumnos; i++) {
			if(alumno[i].id != alumno_id) {
				arregloAux.push(alumno[i]);
			}
		}
		_grupo['alumnos'] = arregloAux;
	}
	function _eliminarDocente() {
	}
	function _eliminarMateria() {
	}
	function _enlistarAlumnos() {
		var alumno = _grupo['alumnos'];
		for(var i = 0; i < alumno.length; i++) {
			console.log(alumno[i].toString);
		}
	}
	function _enlistarAlumnosPorMateria() {
	}
	function _enlistarMaterias() {
		var materias = _grupo['materias'];
		for(var i = 0; i < materias.length; i++) {
			console.log(materias[i].toString);
		}
	}
	return {
		"agregarAlumno" : _agregarAlumno,
		"agregarDocente" : _agregarDocente,
		"agregarMateria" : _agregarMateria,
		"eliminarAlumno" : _eliminarAlumno,
		"eliminarDocente" : _eliminarDocente,
		"eliminarMateria" : _eliminarMateria,
		"enlistarAlumnos" : _enlistarAlumnos,
		"enlistarMaterias" : _enlistarMaterias,
		"enlistarAlumnosPorMateria" : _enlistarAlumnosPorMateria
	}
}
