function Materia (obj){
    var _id = obj.id;
    var _nombre = obj.nombre;
    var _clave = obj.clave;
    var _tipo = "Materia";
    
    function _getId() {
		return _id;
	}
	function _getNombre() {
		return _nombre;
	}
	function _getClave() {
		return _clave;
	}
	function _toString () {
		return "id: " + _id + "\n" +
			"nombre: " + _nombre + "\n" +
			"clave: " + _clave ;
	}
	function tipo() {
		return _tipo;
	}
	return {
		"id" : _getId(),
		"nombre" : _getNombre(),
		"clave" : _getClave(),
		"tipo" : tipo()
	}
}
